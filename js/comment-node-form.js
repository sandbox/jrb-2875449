
/**
 * Based on script here: modules/comment/comment-node-form.js
 */

(function ($) {

    Drupal.behaviors.commentGlobalDisableFieldsetSummaries = {
        attach: function (context) {

            // Provide the summary for the node type form.
            $('fieldset.comment-node-type-settings-form', context).drupalSetSummary(function(context) {

                // Disabled.
                var disabled = $(".form-item-comment-global-disable-disabled input", context).is(':checked');
                if (disabled) {
                    // Disabled-- don't show anything else.
                    return Drupal.checkPlain(Drupal.t('Comments disabled'));
                }

                var vals = [];

                // Default comment setting.
                vals.push($(".form-item-comment select option:selected", context).text());

                // Threading.
                var threading = $(".form-item-comment-default-mode input:checked", context).next('label').text();
                if (threading) {
                    vals.push(threading);
                }

                // Comments per page.
                var number = $(".form-item-comment-default-per-page select option:selected", context).val();
                vals.push(Drupal.t('@number comments per page', {'@number': number}));

                return Drupal.checkPlain(vals.join(', '));
            });
        }
    };

})(jQuery);
